//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>24</ObjektNummer>	GoToSiSy:d:5

#if !defined(h_Led6)
#define h_Led6

// Defines
#define F_CPU 3686400
#define MCU ATMEGA8

#include <avr\io.h>
#include <avr\interrupt.h>

#include <avr\io.h>

class Led6
{

public:

	//	init()	GoToSiSy:d:5|o:25
	void init();

	//	toggle()	GoToSiSy:d:5|o:26
	void toggle();

	//	Konstruktor	GoToSiSy:d:5|o:24
	Led6();

	//	Destruktor	GoToSiSy:d:5|o:24
	~Led6();

private:
	int pin;

protected:


};

#endif
