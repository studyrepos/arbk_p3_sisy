//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>12</ObjektNummer>	GoToSiSy:d:5

#define GeneratedBySisy
#define cpp_Led7
#define SISY_CLASS_NAME Led7
#include "Led7.h"

#include "Controller.h"

extern  Controller app;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:5|o:12
//
/////////////////////////////
Led7::Led7()
{
pin = 7;
	
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:5|o:12
//
/////////////////////////////
Led7::~Led7()
{
	
}
/////////////////////////////
//
//	init()	GoToSiSy:d:5|o:17
//
/////////////////////////////
void Led7::init()
{
	DDRD |= (1 << pin);
	PORTD |= (1 << pin);

} 
/////////////////////////////
//
//	toggle()	GoToSiSy:d:5|o:18
//
/////////////////////////////
void Led7::toggle()
{
	PORTD ^= (1<<pin);

} 

