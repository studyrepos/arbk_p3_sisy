//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>24</ObjektNummer>	GoToSiSy:d:5

#define GeneratedBySisy
#define cpp_Led6
#define SISY_CLASS_NAME Led6
#include "Led6.h"

#include "Controller.h"

extern  Controller app;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:5|o:24
//
/////////////////////////////
Led6::Led6()
{
pin = 6;
	
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:5|o:24
//
/////////////////////////////
Led6::~Led6()
{
	
}
/////////////////////////////
//
//	init()	GoToSiSy:d:5|o:25
//
/////////////////////////////
void Led6::init()
{
	DDRD |= (1 << pin);
	PORTD |= (1 << pin);

} 
/////////////////////////////
//
//	toggle()	GoToSiSy:d:5|o:26
//
/////////////////////////////
void Led6::toggle()
{
	PORTD ^= (1<<pin);

} 

