//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>12</ObjektNummer>	GoToSiSy:d:5

#if !defined(h_Led7)
#define h_Led7

// Defines
#define F_CPU 3686400
#define MCU ATMEGA8

#include <avr\io.h>
#include <avr\interrupt.h>

#include <avr\io.h>

class Led7
{

public:

	//	init()	GoToSiSy:d:5|o:17
	void init();

	//	toggle()	GoToSiSy:d:5|o:18
	void toggle();

	//	Konstruktor	GoToSiSy:d:5|o:12
	Led7();

	//	Destruktor	GoToSiSy:d:5|o:12
	~Led7();

private:
	int pin;

protected:


};

#endif
