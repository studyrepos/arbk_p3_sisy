//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>7</ObjektNummer>	GoToSiSy:d:5

#define GeneratedBySisy
#define cpp_Controller
#define SISY_CLASS_NAME Controller
#include "Controller.h"

#include "Controller.h"

extern  Controller app;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:5|o:7
//
/////////////////////////////
Controller::Controller()
{
	
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:5|o:7
//
/////////////////////////////
Controller::~Controller()
{
	
}
/////////////////////////////
//
//	main()	GoToSiSy:d:5|o:8
//
/////////////////////////////
void Controller::main()
{
	// Diese Operation als Start-Operation f�r main angeben
	
	// hier Initialisierungen durchf�hren
	led6.init();
	led7.init();
	led7.toggle();
	
	// mainloop starten
	run();

} 
/////////////////////////////
//
//	run()	GoToSiSy:d:5|o:9
//
/////////////////////////////
void Controller::run()
{
	do {
	led6.toggle();
	led7.toggle();
	waitMs(50);
	} while (true);
	

} 

