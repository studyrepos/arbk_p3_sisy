//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>7</ObjektNummer>	GoToSiSy:d:5

#if !defined(h_Controller)
#define h_Controller

// Defines
#define F_CPU 3686400
#define MCU ATMEGA8

#include <avr\io.h>
#include <avr\interrupt.h>
#include "Led7.h"
#include "Led6.h"

#include <avr\io.h>

class Controller
{

public:

	//	main()	GoToSiSy:d:5|o:8
	void main();

	//	run()	GoToSiSy:d:5|o:9
	void run();

	Led7 led7;
	Led6 led6;
	//	Konstruktor	GoToSiSy:d:5|o:7
	Controller();

	//	Destruktor	GoToSiSy:d:5|o:7
	~Controller();

private:

protected:


};

#endif
